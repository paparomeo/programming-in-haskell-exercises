merge :: Ord a => [a] -> [a] -> [a]
merge as [] = as
merge [] as = as
merge (a:as) (a':as')
  | a < a' = a : merge as (a' : as')
  | otherwise = a' : merge (a : as) as'
