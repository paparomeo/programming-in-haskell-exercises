merge :: Ord a => [a] -> [a] -> [a]
merge as [] = as
merge [] as = as
merge (a:as) (a':as')
  | a < a' = a : merge as (a' : as')
  | otherwise = a' : merge (a : as) as'

halve :: [a] -> ([a],[a])
halve [a] = ([a], [])
halve [a,a'] = ([a], [a'])
halve (a:a':as) = ((a : bs), (a' : bs'))
  where
    (bs, bs') = halve as

msort :: Ord a => [a] -> [a]
msort [] = []
msort [a] = [a]
msort as = merge (msort bs) (msort bs')
  where
    (bs, bs') = halve as
