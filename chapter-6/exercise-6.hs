and' :: [Bool] -> Bool
and' [b] = b
and' (b:bs) = b && and' bs

concat' :: [[a]] -> [a]
concat' [as] = as
concat' (as:ass) = as ++ concat' ass

replicate' :: Int -> a -> [a]
replicate' 0 _ = []
replicate' n a = a : (replicate' (n - 1) a)

(!!!) :: [a] -> Int -> a
(!!!) (a:_) 1 = a
(!!!) (_:as) n = as !!! (n - 1)

elem' :: Eq a => a -> [a] -> Bool
elem' _ [] = False
elem' a' (a:as) = a' == a || elem' a' as
