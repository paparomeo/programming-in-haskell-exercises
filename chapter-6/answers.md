### Exercise 1
_How does the recursive version of the factorial function behave if applied to a negative argument, such as `(-1)`? Modify the definition to prohibit negative arguments by adding a guard to the recursive case._

#### Answer
The recursive version of the factorial function when applied to a negative argument will result in a stack overflow because it will be invoked with increasingly negative numbers which will never converge, i.e. the base case will never be reached.
```haskell
> fac (-1)
*** Exception: stack overflow
```

Solution in [`exercise-1.hs`](exercise-1.hs) defines a version for the recursive case with a guard that prohibits negative arguments.

### Exercise 2
_Define a recursive function `sumdown :: Int -> Int` that returns the sum of the non-negative integers from a given value down to zero. For example, `sumdown 3` should return the result `3+2+1+0 = 6`._

#### Answer
Solution in [`exercise-2.hs`](exercise-2.hs).

### Exercise 3
_Define the exponentiation operator `^` for non-negative integers using the same pattern of recursion as the multiplication operator `*`, and show how the expression `2 ^ 3` is evaluated using your definition._

#### Answer
```haskell
(^) :: Int -> Int -> Int
m ^ 0 = 1
m ^ n = m * (m ^ (n - 1))
```

```
2 ^ 3
= { applying ^ }
2 * (2 ^ 2)
= { applying ^ }
2 * (2 * (2 ^ 1))
= { applying * }
2 * (2 * (2 * (2 ^ 0)))
={  applying ^ }
2 * (2 * (2 * 1))
= { applying * }
8
```

### Exercise 4
_Define a recursive function `euclid :: Int -> Int -> Int` that implements Euclid’s algorithm for calculating the greatest common divisor of two non-negative integers: if the two numbers are equal, this number is the result; otherwise, the smaller number is subtracted from the larger, and the same process is then repeated. For example:_
```haskell
> euclid 6 27
3
```

#### Answer
Solution in [`exercise-4.hs`](exercise-4.hs).

### Exercise 5
_Using the recursive definitions given in this chapter, show how `length [1,2,3]`, `drop 3 [1,2,3,4,5]`, and `init [1,2,3]` are evaluated._

#### Answer
```
length [1, 2, 3]
= { applying length }
1 + length [2, 3]
= { applying length }
1 + (1 + length [3])
= { applying length }
1 + (1 + (1 + length []))
= { applying length }
1 + (1 + (1 + 0))
= { applying + }
3
```

```
drop 3 [1, 2, 3, 4, 5]
= { applying drop }
drop 2 [2, 3, 4, 5]
= { applying drop }
drop 1 [3, 4, 5]
= { applying drop }
drop 0 [4, 5]
= { applying drop }
[4, 5]
```

```
init [1, 2, 3]
= { applying init }
1 : init [2, 3]
= { applying init }
1 : (2 : init [3])
= { applying init }
1 : (2 : [])
= { applying : }
[1, 2]
```

### Exercise 6
_Without looking at the definitions from the standard prelude, define the following library functions on lists using recursion._
 * _**a.** Decide if all logical values in a list are `True`:_
```haskell
and :: [Bool] -> Bool
```
 * _**b.** Concatenate a list of lists:_
```haskell
concat :: [[a]] -> [a]
```
 * _**c.** Produce a list with `n` identical elements:_
```haskell
replicate :: Int -> a -> [a]
```
 * _**d.** Select the nth element of a list:_
```haskell
(!!) :: [a] -> Int -> a
```
 * _**e.** Decide if a value is an element of a list:_
```haskell
elem :: Eq a => a -> [a] -> Bool
```
_Note: most of these functions are defined in the prelude using other library functions rather than using explicit recursion, and are generic functions rather than being specific to the type of lists._

#### Answer
Solution in [`exercise-6.hs`](exercise-6.hs).

### Exercise 7
_Define a recursive function `merge :: Ord a => [a] -> [a] -> [a]` that merges two sorted lists to give a single sorted list. For example:_
```haskell
> merge [2,5,6] [1,3,4]
[1,2,3,4,5,6]
```
_Note: your definition should not use other functions on sorted lists such as insert or isort, but should be defined using explicit recursion._

#### Answer
Solution in [`exercise-7.hs`](exercise-7.hs).

### Exercise 8
_Using merge, define a function `msort :: Ord a => [a] -> [a]` that implements merge sort, in which the empty list and singleton lists are already sorted, and any other list is sorted by merging together the two lists that result from sorting the two halves of the list separately._

__Hint: first define a function `halve :: [a] -> ([a],[a])` that splits a list into two halves whose lengths differ by at most one._

#### Answer
Solution in [`exercise-8.hs`](exercise-8.hs).

### Exercise 9

_Using the five-step process, construct the library functions that:_
 * _**a.** calculate the sum of a list of numbers;_
 * _**b.** take a given number of elements from the start of a list;_
 * _**c.** select the last element of a non-empty list._

#### Answer

##### a.

###### Step 1: define the type
```haskell
sum :: [Int] -> Int
```

###### Step 2: enumerate the cases
```haskell
sum [] =
sum (n:ns) =
```

###### Step 3: define the simple cases
```haskell
sum [] = 0
sum (n:ns) =
```

###### Step 4: define the other cases
```haskell
sum [] = 0
sum (n:ns) = n + sum ns
```

###### Step 5: generalise and simplify
```haskell
sum :: Num a => [a] -> a
sum [] = 0
sum (n:ns) = n + sum ns
```

##### b.

###### Step 1: define the type
```haskell
take :: Int -> [a] -> [a]
```

###### Step 2: enumerate the cases
```haskell
take 0 [] =
take n [] =
take 0 (a:as) =
take n (a:as) =
```

###### Step 3: define the simple cases
```haskell
take 0 [] = []
take n [] = []
take 0 (a:as) = []
```

###### Step 4: define the other cases
```haskell
take 0 [] = []
take n [] = []
take 0 (a:as) = []
take n (a:as) = a : (take (n - 1) as)
```

###### Step 5: generalise and simplify
```haskell
take :: Integral b => b -> [a] -> [a]
take n []     = []
take 0 (a:as) = []
take n (a:as) = a : (take (n - 1) as)
```

##### c.

###### Step 1: define the type
```haskell
last :: [a] -> a
```

###### Step 2: enumerate the cases
```haskell
last [a] =
last (a:as) =
```

###### Step 3: define the simple cases
```haskell
last [a] = a
```

###### Step 4: define the other cases
```haskell
last [a] = a
last (a:as) = last as
```

###### Step 5: generalise and simplify
```haskell
last :: [a] -> a
last [a] = a
last (a:as) = last as
```
