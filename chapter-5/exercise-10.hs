import Data.Char

let2int :: Char -> Char -> Int
let2int a c = ord c - ord a

int2let :: Char -> Int -> Char
int2let a n = chr (ord a + n)

shift :: Int -> Char -> Char
shift n c
  | isLower c || isUpper c = int2let a ((let2int a c + n) `mod` 26)
  | otherwise = c
  where
    a =
      if isLower c
        then 'a'
        else 'A'

encode :: Int -> String -> String
encode n xs = [shift n x | x <- xs]
