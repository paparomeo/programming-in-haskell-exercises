factors :: Int -> [Int]
factors n = [x | x <- [1 .. n], n `mod` x == 0]

sum_of_factors :: Int -> Int
sum_of_factors n = sum [f | f <- factors n, f /= n]

perfects :: Int -> [Int]
perfects n = [p | p <- [1 .. n], sum_of_factors p == p]
