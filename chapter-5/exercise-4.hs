replicate' :: Int -> x -> [x]
replicate' n x = [x | _ <- [1 .. n]]
