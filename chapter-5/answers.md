### Exercise 1
Using a list comprehension, give an expression that calculates the sum `1² + 2² + … + 100²` of the first one hundred integer squares.

```haskell
> sum [x * x | x <- [1..100]]
338350
> sum [x ^ 2 | x <- [1..100]]
338350
```

### Exercise 2
Suppose that a coordinate grid of size `m × n` is given by the list of all pairs `(x, y)` of integers such that `0 ≤ x ≤ m` and `0 ≤ y ≤ n`. Using a list comprehension, define a function `grid :: Int -> Int -> [(Int,Int)]` that returns a coordinate grid of a given size. For example:
```haskell
> grid 1 2
[(0,0),(0,1),(0,2),(1,0),(1,1),(1,2)]
```

Solution in [`exercise-2.hs`](exercise-2.hs).

### Exercise 3
Using a list comprehension and the function grid above, define a function `square :: Int -> [(Int,Int)]` that returns a coordinate square of size `n`, excluding the diagonal from `(0, 0)` to `(n, n)`. For example:
```haskell
> square 2
[(0,1),(0,2),(1,0),(1,2),(2,0),(2,1)]
```

Solution in [`exercise-3.hs`](exercise-3.hs).

### Exercise 4
In a similar way to the function `length`, show how the library function `replicate :: Int -> a -> [a]` that produces a list of identical elements can be defined using a list comprehension. For example:
```haskell
> replicate 3 True
[True,True,True]
```

Solution in [`exercise-4.hs`](exercise-4.hs).

### Exercise 5
A triple `(x, y, z)` of positive integers is Pythagorean if it satisfies the equation `x² + y² = z²`. Using a list comprehension with three generators, define a function `pyths :: Int -> [(Int,Int,Int)]` that returns the list of all such triples whose components are at most a given limit. For example:
```haskell
> pyths 10
[(3,4,5),(4,3,5),(6,8,10),(8,6,10)]
```

Solution in [`exercise-5.hs`](exercise-5.hs).

### Exercise 6
A positive integer is perfect if it equals the sum of all of its factors, excluding the number itself. Using a list comprehension and the function factors, define a function `perfects :: Int -> [Int]` that returns the list of all perfect numbers up to a given limit. For example:
```haskell
> perfects 500
[6,28,496]
```

Solution in [`exercise-6.hs`](exercise-6.hs).

### Exercise 7
Show how the list comprehension `[(x,y) | x <- [1,2], y <- [3,4]]` with two generators can be re-expressed using two comprehensions with single generators. Hint: nest one comprehension within the other and make use of the library function `concat :: [[a]] -> [a]`.

```haskell
> [(x,y) | x <- [1,2], y <- [3,4]]
[(1,3),(1,4),(2,3),(2,4)]
> concat [[(x,y) | y <- [3,4]] | x <- [1,2]]
[(1,3),(1,4),(2,3),(2,4)]
```

### Exercise 8
Redefine the function `positions` using the function `find`.

Solution in [`exercise-8.hs`](exercise-8.hs).

### Exercise 9
The scalar product of two lists of integers `xs` and `ys` of length `n` is given by the sum of the products of corresponding integers:
```math
\sum_{i=0}^{n-1} (xs_{i} \times ys_{i})
```
In a similar manner to `chisqr`, show how a list comprehension can be used to define a function `scalarproduct :: [Int] -> [Int] -> Int` that returns the scalar product of two lists. For example:
```
> scalarproduct [1,2,3] [4,5,6]
32
```

Solution in [`exercise-9.hs`](exercise-9.hs).

### Exercise 10
Modify the Caesar cipher program to also handle upper-case letters.


Solution in [`exercise-10.hs`](exercise-10.hs).
