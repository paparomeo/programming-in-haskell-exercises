# 1. Work through the examples from this chapter using GHCi.

```
*Main> 2+3*4
14
*Main> (2+3)+4
9
*Main> (2+3)*4
20
*Main> sqrt(3^2 + 4^2)
5.0
*Main> head [1,2,3,4,5]
1
*Main> tail [1,2,3,4,5]
[2,3,4,5]
*Main> [1,2,3,4,5] !! 2
3
*Main> drop 3 [1,2,3,4,5]
[4,5]
*Main> length [1,2,3,4,5]
5
*Main> sum [1,2,3,4,5]
15
*Main> product [1,2,3,4,5]
120
*Main> [1,2,3] ++ [4,5]
[1,2,3,4,5]
*Main> reverse [1,2,3,4,5]
[5,4,3,2,1]
```

See also [`exercise-1.hs`](exercise-1.hs).

# 2. Parenthesise the following numeric expressions:

```
2^3*4 
(2^3)*4

2*3+4*5 
(2*3)+(4*5)

2+3*4^5
2+(3*(4^5))
```

# 3. The script below contains three syntactic errors. Correct these errors and then check that your script works properly using GHCi.

```
N = a `div` length xs
  where
    a = 10
    xs = [1,2,3,4,5]
```

Solution in [`exercise-3.hs`](exercise-3.hs).

# 4. The library function last selects the last element of a non-empty list; for example, `last [1,2,3,4,5] = 5`. Show how the function last could be defined in terms of the other library functions introduced in this chapter. Can you think of another possible definition?

Solutions in [`exercise-4.hs`](exercise-4.hs).

# 5. The library function init removes the last element from a non-empty list; for example, `init [1,2,3,4,5] = [1,2,3,4]`. Show how init could similarly be defined in two different ways.

Solutions in [`exercise-5.hs`](exercise-5.hs).
