# 1. What are the types of the following values?

```
['a','b','c'] :: [Char]

('a','b','c') :: (Char,Char,Char)

[(False,'0'),(True,'1')] :: [(Bool,Char)]

([False,True],['0','1']) :: ([Bool],[Char])

[tail, init, reverse] :: [[a] -> [a]]
```

# 2. Write down definitions that have the following types; it does not matter what the definitions actually do as long as they are type correct.

```
bools :: [Bool]
bools = [True,False,True]

nums :: [[Int]]
nums = [[1::Int,2,3], [4], [5,6], [7,8,9,10]]

add :: Int -> Int -> Int -> Int
add x y z = x + y + z

copy :: a -> (a,a)
copy x = (x,x)

apply :: (a -> b) -> a -> b
apply x y = x y
```

# 3. What are the types of the following functions?

```
second :: [a] -> a
second xs = head (tail xs)

swap :: (a,b) -> (b,a)
swap (x,y) = (y,x)

pair :: a -> b -> (a,b)
pair x y = (x,y)

double :: Num a => a -> a
double x = x*2

palindrome :: Eq a => [a] -> Bool
palindrome xs = reverse xs == xs

twice :: (a -> a) -> a -> a
twice f x = f (f x)
```

# 4. Check your answers to the preceding three questions using GHCi.

```
Prelude> :type ['a','b','c'] 
['a','b','c'] :: [Char]
Prelude> :type ('a','b','c')
('a','b','c') :: (Char, Char, Char)
Prelude> :type [(False,'0'),(True,'1')]
[(False,'0'),(True,'1')] :: [(Bool, Char)]
Prelude> :type ([False,True],['0','1'])
([False,True],['0','1']) :: ([Bool], [Char])
Prelude> :type [tail, init, reverse]
[tail, init, reverse] :: [[a] -> [a]]

Prelude> bools = [True,False,True]
Prelude> :type bools
bools :: [Bool]
Prelude> nums = [[1::Int,2,3], [4], [5,6], [7,8,9,10]]
Prelude> :type nums
nums :: [[Int]]
Prelude> add x y z = x + y + z
Prelude> :type add
add :: Num a => a -> a -> a -> a
Prelude> copy x = (x,x)
Prelude> :type copy
copy :: b -> (b, b)
Prelude> apply x y = x y
Prelude> :type apply
apply :: (t1 -> t2) -> t1 -> t2

Prelude> second xs = head (tail xs)
Prelude> :type second
second :: [a] -> a
Prelude> swap (x,y) = (y,x)
Prelude> :type swap
swap :: (b, a) -> (a, b)
Prelude> pair x y = (x,y)
Prelude> :type pair
pair :: a -> b -> (a, b)
Prelude> double x = x*2
Prelude> :type double
double :: Num a => a -> a
Prelude> palindrome xs = reverse xs == xs
Prelude> :type palindrome 
palindrome :: Eq a => [a] -> Bool
Prelude> twice f x = f (f x)
Prelude> :type twice
twice :: (t -> t) -> t -> t
```

# 5. Why is it not feasible in general for function types to be instances of the Eq class? When is it feasible? Hint: two functions of the same type are equal if they always return equal results for equal arguments.

Because in general, it's not feasible to compare functions for equality. I believe it is feasible only for functions with finite (bounded) argument types.
