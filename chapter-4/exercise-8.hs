luhnDouble :: Int -> Int
luhnDouble n =
  if d > 9
    then d - 9
    else d
  where
    d = n * 2

luhn :: Int -> Int -> Int -> Int -> Bool
luhn a b c d = (luhnDouble a + b + luhnDouble c + d) `mod` 10 == 0
