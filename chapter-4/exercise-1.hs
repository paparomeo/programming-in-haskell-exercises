halve :: [a] -> ([a], [a])
halve xs = (take h xs, drop h xs)
  where
    h = length xs `div` 2
