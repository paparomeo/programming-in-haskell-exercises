# 1. Give another possible calculation for the result of `double (double 2)`.

```
double (double 2)
= { applying the outer double }
(double 2) + (double 2)
= { applying first double }
(2 + 2) + (double 2)
= { applying second double }
(2 + 2) + (2 + 2)
= { applying the first inner + }
4 + (2 + 2)
= { applying the second inner + }
4 + 4
= { applying the + }
8
```

# 2. Show that `sum [x] = x` for any number `x`.

```
sum [x]
= { applying sum }
x + sum []
= { applying sum }
x + 0
= { applying + }
x
```

# 3. Define a function product that produces the product of a list of numbers, and show using your definition that `product [2,3,4] = 24`.

```
product []     = 1
product (n:ns) = n * product ns
```

```
product [2,3,4]
= { applying product }
2 * product [3,4]
= { applying product }
2 * (3 * product[4])
= { applying product }
2 * (3 * (4 * 1))
= { applying * }
2 * (3 * 4))
= { applying * }
2 * 12
= { applying * }
24
```

# 4. How should the definition of the function `qsort` be modified so that it produces a reverse sorted version of a list?

```
qrsort []     = []
qrsort (x:xs) = qrsort larger ++ [x] ++ qrsort smaller
               where
                 smaller = [a | a <- xs, a <= x]
                 larger = [b | b <- xs, b > x]
```

# 5. What would be the effect of replacing <= by < in the original definition of qsort? Hint: consider the example qsort [2,2,3,1,1].

It would sort the list but also remove the duplicates.

```
qsort' [] = []
qsort' (x:xs) = qsort' smaller ++ [x] ++ qsort' larger
               where
                 smaller = [a | a <- xs, a < x]
                 larger = [b | b <- xs, b > x]
```

```
qsort [2,2,3,1,1]
={ applying qsort' }
qsort [1,1] ++ [2] ++ qsort' [3]
={ applying qsort' with qsort [x] = [x] property}
(qsort [] ++ [1] ++ qsort []) ++ [2] ++ [3]
={ applying qsort', above property}
([] ++ [1] ++ []) ++ [2] ++ [3]
={ applying ++ }
[1] ++ [2] ++ [3]
={ applying ++ }
[1,2,3]
```
